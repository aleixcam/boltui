module.exports = {
  stories: [
	  '../src/**/*.story.js',
	  '../src/**/*.stories.js'
  ],
  addons: ['@storybook/addon-actions', '@storybook/addon-links'],
};
