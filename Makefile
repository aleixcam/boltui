all: help

## up: Create and start the container
up:
	docker-compose up -d

## down: Stop and remove the container
down:
	docker-compose down

## shell: Interactive shell to use commands inside the container
watch:
	docker-compose exec node npm run sass:watch

## shell: Interactive shell to use commands inside the container
shell:
	docker-compose run --rm node sh

## pull: Update docker image
pull:
	docker-compose pull

## help: Show this screen
help: Makefile
	@sed -n 's/^##//p' $<
