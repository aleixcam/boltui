import React from 'react'
import Header from './Header'
import { action } from '@storybook/addon-actions'

export default {
	title: 'Header',
	component: Header
}

const album = {
	cover: 'https://upload.wikimedia.org/wikipedia/en/0/06/Sabaton_Carolus_Rex.jpg',
	artist: 'Sabaton',
	album: 'Carolus Rex',
	genre: 'Power Metal',
	year: '2012'
}

const titles = ['artist', 'genre', 'year']

export const Default = () => (
	<Header onPlay={action('played')} onShuffle={action('shuffled')} title={album.album}>
		{titles.map((title, index) => <span key={index}>{album[title]}</span>)}
	</Header>
)
