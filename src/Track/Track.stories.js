import React from 'react'
import Track from './Track'
import { action } from '@storybook/addon-actions'

export default {
	title: 'Track',
	component: Track
}

const track = {
	cover: 'https://upload.wikimedia.org/wikipedia/en/0/06/Sabaton_Carolus_Rex.jpg',
	title: 'Gott Mit Uns',
	artist: 'Sabaton'
}

export const Unloaded = () => <Track track={track} active={false} onPlay={action('played')} />

export const Loaded = () => <Track track={{ ...track, data: {} }} active={false} onPlay={action('played')} />

export const Active = () => <Track track={{ ...track, data: {} }} active={true} onPlay={action('played')} />
