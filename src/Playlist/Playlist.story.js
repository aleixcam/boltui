import React from 'react'
import Playlist from './Playlist'
import { action } from '@storybook/addon-actions'

export default {
	title: 'Playlist',
	component: Playlist
}

const playlist = [
	{
		id: 1,
		cover: 'https://upload.wikimedia.org/wikipedia/en/0/06/Sabaton_Carolus_Rex.jpg',
		title: 'Gott Mit Uns',
		artist: 'Sabaton',
		album: 'Carolus Rex',
		data: {}
	},
	{
		id: 2,
		cover: 'https://upload.wikimedia.org/wikipedia/en/0/06/Sabaton_Carolus_Rex.jpg',
		title: '1 6 4 8',
		artist: 'Sabaton',
		album: 'Carolus Rex',
		data: {}
	},
	{
		id: 3,
		cover: 'https://upload.wikimedia.org/wikipedia/en/0/06/Sabaton_Carolus_Rex.jpg',
		title: 'Poltava',
		artist: 'Sabaton',
		album: 'Carolus Rex'
	},
]

export const Default = () => (
	<Playlist playlist={playlist}
		current={playlist[1]}
		onChangeTrack={action('changed track')}
		onClose={action('closed')} />
)
