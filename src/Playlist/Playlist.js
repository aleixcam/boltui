import React from 'react'
import { Current, Track } from '..'
import './Playlist.css'

const Playlist = props => {
    return <nav className="playlist">
		<Current track={props.current} onClick={props.onClose} />
        <ul className="playlist__menu">
            {props.playlist && props.playlist.length > 0 && (
                props.playlist.map(track => (
					<Track key={track.id}
						track={track}
						active={track.id === props.current.id}
						onPlay={props.onChangeTrack} />
				))
            )}
        </ul>
    </nav>
}

export default Playlist
