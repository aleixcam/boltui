import React from 'react'
import PlaylistControls from './PlaylistControls'
import { action } from '@storybook/addon-actions'

export default {
	title: 'PlaylistControls',
	component: PlaylistControls
}

export const Default = () => (
	<PlaylistControls hasPlaylist={true}
		shuffle={0}
		repeat={0}
		onShuffle={action('shuffled')}
		onRepeat={action('repeated')}
		onClosePlaylist={action('closed playlist')} />
)

export const WithoutPlaylist = () => (
	<PlaylistControls hasPlaylist={false}
		shuffle={0}
		repeat={0}
		onShuffle={action('shuffled')}
		onRepeat={action('repeated')}
		onClosePlaylist={action('closed playlist')} />
)

export const RepeatOne = () => (
	<PlaylistControls hasPlaylist={true}
		shuffle={0}
		repeat={2}
		onShuffle={action('shuffled')}
		onRepeat={action('repeated')}
		onClosePlaylist={action('closed playlist')} />
)
