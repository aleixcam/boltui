import React from 'react'
import './PlaylistControls.css'
import '../../static/fonts/fontawesome.min.css'

const PlaylistControls = props => {
    return <section className="playlist-controls">
		<button className={'playlist-controls__button' + (props.shuffle ? ' playlist-controls__button--active' : '')}
			onClick={props.onShuffle}>
			<span className="fas fa-random"></span>
		</button>
		<button className={'playlist-controls__button' + (props.repeat ? ' playlist-controls__button--active' : '')}
			onClick={props.onRepeat}>
			<span className={'fas fa-sync-alt' + (props.repeat === 2 ? ' one' : '')}></span>
		</button>
		<button className={'playlist-controls__button'}
			onClick={props.onClosePlaylist}
			disabled={!props.hasPlaylist}>
			<span className="fas fa-chevron-down"></span>
		</button>
	</section>
}

export default PlaylistControls
