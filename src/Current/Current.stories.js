import React from 'react'
import Current from './Current'
import { action } from '@storybook/addon-actions'

export default {
	title: 'Current',
	component: Current
}

const track = {
	cover: 'https://upload.wikimedia.org/wikipedia/en/0/06/Sabaton_Carolus_Rex.jpg',
	title: 'Gott Mit Uns',
	artist: 'Sabaton',
	album: 'Carolus Rex'
}

export const Folded = () => <Current track={track} folded={true} onClick={action('clicked')} />

export const Unfolded = () => <Current track={track} folded={false} onClick={action('clicked')} />
