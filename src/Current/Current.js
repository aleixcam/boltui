import React from 'react'
import './Current.css'

const Current = props => {
    return <section className={'current' + (props.folded ? '' : ' current--header')} onClick={props.onClick}>
		<div className="current__image">
			<div style={{ backgroundImage: 'url("'+props.track.cover+'")' }}></div>
		</div>
		<div className="current__text">
			<h1>{props.track.title || props.track.filename || '\xa0'}</h1>
			<p>{props.track.artist || '\xa0'}</p>
			<p>{props.track.album || '\xa0'}</p>
		</div>
	</section>
}

export default Current
