import React from 'react'
import './Sidenav.css'

const Sidenav = props => {
    return <nav className="sidenav">
        <ul className="sidenav__menu">
            {props.children && props.children.length > 0 && props.children.map(link => (
					React.cloneElement(link, {
						key: link.props.view,
						className: 'sidenav__link' + (link.props.link === props.active ? ' sidenav__link--active' : ''),
						onClick: () => props.onGoToView(link.props.link)
					 })
				))
            }
        </ul>
    </nav>
}

export default Sidenav
