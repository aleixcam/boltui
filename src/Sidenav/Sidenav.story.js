import React, { useState } from 'react'
import Sidenav from './Sidenav'

export default {
	title: 'Sidenav',
	component: Sidenav
}


export const Default = () => {
	const [active, setActive] = useState('artist')

	return <Sidenav onGoToView={view => setActive(view)} active={active}>
		<li link="artist">Albums<span className="fas fa-square"></span></li>
		<li link="artists">Artists<span className="fas fa-male"></span></li>
		<li link="songs">Songs<span className="fas fa-music"></span></li>
		<li link="genre">Genres<span className="fas fa-drum"></span></li>
		<li link="year">Years<span className="far fa-calendar"></span></li>
	</Sidenav>
}
