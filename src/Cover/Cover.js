import React, { useState } from 'react'
import './Cover.css'

const Cover = props => {
    return <article className="cover" onDoubleClick={props.onPlay}>
        <div className="cover__image">
            <div className={props.selected ? 'selected' : 'selectable'}
				style={{backgroundImage: 'url("'+props.album.cover+'")'}} />
        </div>
        <small className="cover__title cover__title--small">{props.album.artist}</small>
        <h4 className="cover__title">{props.album.album}</h4>
    </article>
}

export default Cover
