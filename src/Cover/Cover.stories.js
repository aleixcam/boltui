import React from 'react'
import Cover from './Cover'
import { action } from '@storybook/addon-actions'

export default {
	title: 'Cover',
	component: Cover
}

const album = {
	cover: 'https://upload.wikimedia.org/wikipedia/en/0/06/Sabaton_Carolus_Rex.jpg',
	artist: 'Sabaton',
	album: 'Carolus Rex'
}

export const Unselected = () => <Cover album={album} onPlay={action('played')} selected={false} />

export const Selected = () => <Cover album={album} onPlay={action('played')} selected={true} />
