import React from 'react'
import PlayerControls from './PlayerControls'
import { action } from '@storybook/addon-actions'

export default {
	title: 'PlayerControls',
	component: PlayerControls
}

export const Default = () => (
	<PlayerControls isPlaying={false}
		onPlay={action('played')}
		onPause={action('paused')}
		onPreviousTrack={action('changed to previous track')}
		onNextTrack={action('changed to next track')} />
)

export const Playing = () => (
	<PlayerControls isPlaying={true}
		onPlay={action('played')}
		onPause={action('paused')}
		onPreviousTrack={action('changed to previous track')}
		onNextTrack={action('changed to next track')} />
)
