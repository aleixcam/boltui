import React from 'react'
import './PlayerControls.css'
import '../../static/fonts/fontawesome.min.css'

const PlayerControls = props => {
    return <section className="player-controls">
		<button className="player-controls__button player-controls__button--small" onClick={props.onPreviousTrack}>
			<span className="fas fa-backward"></span>
		</button>
		{props.isPlaying
			? <button className="player-controls__button" onClick={props.onPause}>
				<span className="fas fa-pause"></span>
			</button>
			: <button className="player-controls__button" onClick={props.onPlay}>
				<span className="fas fa-play"></span>
			</button>
		}
		<button className="player-controls__button player-controls__button--small" onClick={props.onNextTrack}>
			<span className="fas fa-forward"></span>
		</button>
	</section>
}

export default PlayerControls
