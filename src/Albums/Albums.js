import React, { useEffect, useRef } from 'react'
import { Cover } from '..'
import './Albums.css'

const Albums = props => {
	const albumsSection = useRef()

	useEffect(() => {
		align()

		window.addEventListener('resize', align)

		return () => {
			window.removeEventListener('resize', align)
		}
	}, [])

	const align = () => {
		const style = getComputedStyle(albumsSection.current)
		const width = albumsSection.current.offsetWidth
			- (parseFloat(style.paddingLeft) + parseFloat(style.paddingRight))

		const columns = Math.floor(width / 240)

		albumsSection.current.style.gridTemplateColumns = `repeat(${columns}, ${columns}fr)`
	}

    return <section className="albums" ref={albumsSection}>
        {props.albums && props.albums.length > 0 && (
            props.albums.map((album, index) => (
                <Cover key={index}
					album={album} 
					selected={props.selected.includes(album.id)}
					onPlay={props.onPlay} />
			))
        )}
    </section>
}

export default Albums
