import React from 'react'
import Albums from './Albums'
import { action } from '@storybook/addon-actions'

export default {
	title: 'Albums',
	component: Albums
}

const albums = [
	{
		id: 1,
		cover: 'http://www.berritxarrak.net/wp-content/uploads/2014/10/btx03eskuak.jpg',
		artist: 'Berri Txarrak',
		album: 'Eskuak/Ukabilak'
	},
	{
		id: 2,
		cover: 'https://www.elquintobeatle.com/wp-content/uploads/2015/01/muse-origin-of-symmetry-1.jpg',
		artist: 'Muse',
		album: 'Origin of Symmetry'
	},
	{
		id: 3,
		cover: 'https://www.lacoccinelle.net/590899-1.jpg?20191117',
		artist: 'Rise Against',
		album: 'Siren Song of the Counter Culture'
	},
	{
		id: 4,
		cover: 'https://upload.wikimedia.org/wikipedia/en/0/06/Sabaton_Carolus_Rex.jpg',
		artist: 'Sabaton',
		album: 'Carolus Rex'
	},
	{
		id: 5,
		cover: 'https://images-na.ssl-images-amazon.com/images/I/81O5BH0QI4L._SX466_.jpg',
		artist: 'System of a Down',
		album: 'Toxicity'
	}
]

export const Default = () => (
	<Albums albums={albums}
		selected={[2, 4]}
		onPlay={action('played')}
		onAlign={action('aligned')} />
)
