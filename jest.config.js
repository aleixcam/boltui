module.exports = {
	setupFilesAfterEnv: ["<rootDir>/config/jest.setup.js"],
	moduleNameMapper: {
		"\\.(css|less|sass|scss)$": "<rootDir>/__mocks__/style.js",
		"\\.(gif|ttf|eot|svg)$": "<rootDir>/__mocks__/file.js"
    },
}
